import numpy as np
import matplotlib.pyplot as plt
from numba import jit

@jit(nopython=True)
def compute(x, y, N, lmda, omega, dt, sigma):
    randX = np.random.randn(N)
    randY = np.random.randn(N)
    x[0] = 1
    y[0] = 4
    for i in range(N):
        x[i + 1] = x[i] - (lmda * x[i] + omega * y[i]) * dt + sigma * randX[i] * np.sqrt(dt)
        y[i + 1] = y[i] + (omega * x[i] - lmda * y[i]) * dt + sigma * randY[i] * np.sqrt(dt)


if __name__ == "__main__":
    fig, axs = plt.subplots(2, 2)
    dt = 0.01
    lmda = 0.3
    omega = 2.0
    #sigma = 5. #варьировать
    sigmas = [0., 0.5, 1.5, 5., 20]

    start = 0
    end = 10
    N = int((end - start) / dt)
    for sigma in sigmas:
        x = np.zeros(N + 1)
        y = np.zeros(N + 1)
        compute(x, y, N, lmda, omega, dt, sigma)
        plot1 = plt.subplot2grid((3, 2), (0, 0))
        plot2 = plt.subplot2grid((3, 2), (0, 1))
        plot3 = plt.subplot2grid((3, 2), (1, 0), colspan=2, rowspan=2)
        plt.suptitle(f'sigma = {sigma}')
        plot1.plot(x)
        plot1.set_title("X")
        plot2.plot(y)
        plot2.set_title("Y")
        plot3.plot(x, y)

        plt.show()
